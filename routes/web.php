<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('user.register');
});
Route::post('/', 'UserController@register')->name('register');

Route::get('login', function () {
    return view('user.login');
})->name('login');
Route::post('login', 'UserController@login')->name('login-final');

Route::group(['middleware' => 'auth'], function(){
	Route::get('/list', 'UserController@index')->name('category.list');
	Route::get('add-category', 'UserController@add')->name('add-category');
	Route::post('create-category', 'UserController@create')->name('create-category');
	Route::get('edit-category/{id}', 'UserController@edit')->name('edit-category');
	Route::post('update-category/{id}', 'UserController@update')->name('update-category');
	Route::get('delete-data/{id}', 'UserController@delete')->name('delete-category');
});


