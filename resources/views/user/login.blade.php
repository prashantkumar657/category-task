<!DOCTYPE html>
<html lang="en">

	<head>
	  	<!-- Required meta tags -->
	  	<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Task Admin</title>
		<!-- plugins:css -->
	  	<link rel="stylesheet" href="{{asset('adminasset/vendors/iconfonts/mdi/css/materialdesignicons.min.css')}}">
	  	<link rel="stylesheet" href="{{asset('adminasset/vendors/css/vendor.bundle.base.css')}}">
	  	<!-- endinject -->
	  	<!-- plugin css for this page -->
	  	<!-- End plugin css for this page -->
	  	<!-- inject:css -->
	  	<link rel="stylesheet" href="{{asset('adminasset/css/style.css')}}">
	  	<!-- endinject -->
	  	<link rel="shortcut icon" href="{{asset('adminasset/images/favicon.png')}}" />

	  	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.css">
	</head>

	<body base-url="{{ asset('/') }}">
	  	<div class="container-scroller">
			<div class="container-fluid page-body-wrapper full-page-wrapper">
			  	<div class="content-wrapper d-flex align-items-center auth">
					<div class="row w-100">
					  	<div class="col-lg-4 mx-auto">
							<div class="auth-form-light text-left p-5">
							  	<div class="brand-logo">
									<!-- <img src="{{asset('adminasset/images/logo.svg')}}"> -->
							  	</div>
							  	<h4>Task : User</h4>
							  	<h6 class="font-weight-light">Sign in to continue.</h6>
							  	<form class="pt-3" method="POST" action="{{ route('login-final') }}" >
									@csrf
									<div class="form-group">
									  	<input type="email" name="email" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="Username">
									  	@if($errors->has('email'))
									  	<small id="passwordHelp" class="text-danger">
							              	{{$errors->first('email')}}
							            </small>
							            @endif
									</div>
									<div class="form-group">
									  	<input type="password" name="password" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Password">
									  	@if($errors->has('password'))
									  	<small id="passwordHelp" class="text-danger">
							              	{{$errors->first('password')}}
							            </small>
							            @endif
									</div>
									<div class="mt-3">
									  	<input type="submit" class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn" id="form-submit" name="" value="SIGN IN">
									</div>
							  	</form>
							</div>
					  	</div>
					</div>
			  	</div>
			  <!-- content-wrapper ends -->
			</div>
		<!-- page-body-wrapper ends -->
	  	</div>
	  	<!-- container-scroller -->
	  	<!-- plugins:js -->
	  	<script src="{{asset('adminasset/vendors/js/vendor.bundle.base.js')}}"></script>
	  	<script src="{{asset('adminasset/vendors/js/vendor.bundle.addons.js')}}"></script>
	  	<!-- endinject -->
	  	<!-- inject:js -->
	 	<script src="{{asset('adminasset/js/off-canvas.js')}}"></script>
	  	<script src="{{asset('adminasset/js/misc.js')}}"></script>

	  	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.js"></script>
	  	<!-- endinject -->
	</body>

</html>
