<!-- plugins:css -->
	<link rel="stylesheet" href="{{asset('adminasset/vendors/iconfonts/mdi/css/materialdesignicons.min.css')}}">
	<link rel="stylesheet" href="{{asset('adminasset/vendors/css/vendor.bundle.base.css')}}">
	<!-- endinject -->
	<!-- plugin css for this page -->
	<!-- End plugin css for this page -->
	<!-- inject:css -->
	<link rel="stylesheet" href="{{asset('adminasset/css/style.css')}}">
	<!-- endinject -->
	<link rel="shortcut icon" href="{{asset('adminasset/images/favicon.png')}}" />