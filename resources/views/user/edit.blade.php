@extends('layouts.user')
@section('title', 'User List')

@section('content')
    @if($errors->count() > 0)
        @foreach($errors->all() as $error)
            <p class="alert alert-danger">{{$error}}</p>
        @endforeach
    @endif
    @if(Session::has('message'))
        <div class="alert alert-success"> {{Session::get('message')}}</div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger"> {{Session::get('error')}}</div>
    @endif
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Edit</h4>
                <p class="card-description">
                    <!-- Add class <code>.table-striped</code> -->
                </p>
                <form action="{{ route('update-category',$check->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group">
                        <label for="f_name">Name:</label>
                        <input type="text" class="form-control" name="name" value="{{ $check->name }}">
                    </div>

                    <div class="form-group">
                        <img src="{{ asset($check->image) }}">
                        <label for="f_name">Choose Pic:</label>
                        <input type="file" class="form-control" name="image" id="image">
                    </div>

                    <button type="submit" class="btn btn-default">Update</button>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
<script>
    var base_url = $('body').attr('data-baseurl');
</script>
@endsection