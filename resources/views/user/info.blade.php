@extends('layouts.user')
@section('title', 'User List')

@section('content')
    @if($errors->count() > 0)
        @foreach($errors->all() as $error)
            <p class="alert alert-danger">{{$error}}</p>
        @endforeach
    @endif
    @if(Session::has('message'))
        <div class="alert alert-success"> {{Session::get('message')}}</div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger"> {{Session::get('error')}}</div>
    @endif
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="col-lg-12">
            </div>
            <br>
            <div class="col-lg-12">
            </div>
            
            <div class="card-body table-responsive">
                @if($parent_detail == null)
                    <a href="{{ route('add-category') }}" class="btn btn-warning" style="float: right;">Add Category </a>
                @else
                    <a href="{{ route('add-category', ['category' => $parent_detail->id]) }}" class="btn btn-warning" style="float: right;">Add Sub-Category for {{ $parent_detail->name }} </a>
                @endif

                <h4 class="card-title">List</h4>
                <p class="card-description">
                    <!-- Add class <code>.table-striped</code> -->
                </p>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>S.No.</th>
                            <th>Name</th>
                            <th>Image</th>
                            <th>Number Of Suc Cat</th>
                            <th>Time</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($categories->count() > 0)
                            @foreach($categories as $key => $value)
                                <tr>
                                    <td>{{ $loop->index+1 }}</td>
                                    <td>{{ $value->name }}</td>
                                    <td>
                                        <a href="{{ asset($value->image) }}" target="_blank">View</a>
                                    </td>
                                    <td>{{ $value->sub_categories_count }}</td>
                                    <td>{{ $value->created_at }}</td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-primary dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Action
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu pull-right" role="menu" aria-labelledby="menu1">

                                                <li role="presentation">
                                                    <a role="menuitem" tabindex="-1" href="{{ route('category.list', ['category' => $value->id]) }}"><span class="fa fa-pencil-square-o"></span> View</a>
                                                </li>

                                                <li role="presentation">
                                                    <a role="menuitem" tabindex="-1" href="{{ route('edit-category',$value->id) }}"><span class="fa fa-pencil-square-o"></span> Edit</a>
                                                </li>

                                                <li role="presentation">
                                                    <a role="menuitem" tabindex="-1" href="{{ route('delete-category',$value->id) }}"><span class="fa fa-trash"></span> Delete</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="10" style="text-align: center; font-size: 30px;">No Details Available </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            {{$categories->appends(request()->input())->links()}}
        </div>
    </div>

@endsection

@section('scripts')

@endsection