@extends('layouts.user')
@section('title', 'Category List')

@section('content')
    @if($errors->count() > 0)
        @foreach($errors->all() as $error)
            <p class="alert alert-danger">{{$error}}</p>
        @endforeach
    @endif
    @if(Session::has('message'))
        <div class="alert alert-success"> {{Session::get('message')}}</div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger"> {{Session::get('error')}}</div>
    @endif
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                @if($parent_detail == null)
                    <h4 class="card-title">Add Category</h4>
                @else
                    <h4>Add Sub-Category for {{ $parent_detail->name }}</h4>
                @endif
                <p class="card-description">
                    <!-- Add class <code>.table-striped</code> -->
                </p>
                <form action="{{ route('create-category') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="parent_id" value="{{ request()->input('category') }}">
                    <div class="form-group">
                        <label for="f_name">Name:</label>
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                    </div>
                    <div class="form-group">
                        <label for="f_name">Choose Pic:</label>
                        <input type="file" class="form-control" name="image" id="image">
                    </div>
                    <button type="submit" class="btn btn-default" id="form-submit">Submit</button>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

@endsection