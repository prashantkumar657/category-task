<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Category;

class UserController extends Controller
{
    public function register(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'mobile' => 'required|numeric|unique:users,mobile',
            'password' => 'required|min:6|confirmed',
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->password = bcrypt($request->password);
        $user->save();

        return redirect()->route('login')->with('message', 'Registered Successfully.');
    }

    public function login(Request $request){
        $this->validate($request, [
            'email' => 'required|email|exists:users,email',
            'password' => 'required',
        ]);

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            return redirect()->route('category.list');
        }else{
            return redirect()->back()->with('error', 'Email and Password does not match.');
        }
    }

    public function index(Request $request){
        // dd($request);
    	$categories = Category::orderBy('id', 'desc');
        if(!isset($request->category) && $request->category == null){
            $categories = $categories->whereNull('parent_id');
            $parent_detail = '';
        }else{
            $categories = $categories->where('parent_id', $request->category);
            $parent_detail = Category::where('id', $request->category)->first();
        }
        $categories = $categories->withCount('subCategories')->paginate(10);
        // dd($categories);
    	return view('user.info', compact('categories', 'parent_detail'));
    }

    public function add(){
        if(!isset(request()->category) && request()->category == null){
            $parent_detail = '';
        }else{
            $parent_detail = Category::where('id', request()->category)->first();
        }
    	return view('user.add', compact('parent_detail'));
    }

    public function create(Request $request){
    	$this->validate($request, [
    		'name' => 'required|unique:categories,name',
    		'image' => 'required|image|mimes:jpg,png,jpeg,gif,webp,svg|max:300|dimensions:min_width=100,min_height=100,max_width=1500,max_height=1500',
    	]);

        if(isset($request->parent_id) && $request->parent_id != null){
            $check = Category::where('id', $request->parent_id)->first();
            if($check == null){
                return redirect()->back()->with('error', 'Parent Category does not exists');
            }
        }

    	$query = new Category();
    	$query->name = $request->name;
    	if($request->hasFile('image')){
    		$path = 'storage/'.$request->file('image')->store('profile-pic/'.date('d-m-Y'));
    		$query->image = $path;
    	}
        $query->parent_id = $request->parent_id;
    	$query->save();
        return redirect()->route('category.list',['category' => $request->parent_id])->with('message', 'Created Successfully.');
    }

    public function edit($id){
    	$check = Category::where('id', $id)->first();
    	if($check == null){
    		return redirect()->back()->with('error', 'Data does not exists.');
    	}
    	return view('user.edit', compact('check'));
    }

    public function update(Request $request, $id){
        $check = Category::where('id', $id)->first();
        if($check == null){
            return redirect()->back()->with('error', 'Data does not exists.');
        }

        $this->validate($request, [
            'name' => 'required|unique:categories,name',
            'image' => 'nullable|image|mimes:jpg,png,jpeg,gif,webp,svg|max:300|dimensions:min_width=100,min_height=100,max_width=1500,max_height=1500',
        ]);

    	$query = $check;
        $query->name = $request->name;
        if($request->hasFile('image')){
            $path = 'storage/'.$request->file('image')->store('profile-pic/'.date('d-m-Y'));
            $query->image = $path;
        }
        $query->parent_id = $request->parent_id;
        $query->save();

    	return redirect()->route('category.list',['category' => request()->input('category')])->with('message', 'Category Updated Successfully.');
    }

    public function delete($id){
        $check = Category::where('id', $id)->first();
        if($check == null){
            return redirect()->back()->with('error', 'Data does not exists.');
        }

    	$check->delete();
    	return redirect()->route('category.list',['category' => request()->input('category')])->with('message', 'Data Deleted Successfully.');
    }
}
